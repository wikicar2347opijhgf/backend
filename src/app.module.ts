import { Module } from '@nestjs/common';
import { SmartphoneModule } from './smartphone/smartphone.module';
import { TypeOrmModule } from '@nestjs/typeorm';
@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5433,
      username: 'samuelmer',
      password: 'hola123.',
      database: 'smartphonedb',
      entities: ['dist/**/*.entity{.ts,.js}'],
      synchronize: true,
      retryDelay: 3000,
      retryAttempts: 5,
    }),
    SmartphoneModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
