import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { SmartphoneService } from '../services/smartphone.service';
import { Smartphone } from '../entities/smartphone.entity';
@Controller('api/smartphone')
export class SmartphoneController {
  constructor(private smartService: SmartphoneService) {}
  @Get()
  async getAll(): Promise<Smartphone[]> {
    return this.smartService.findAll();
  }
  @Get(':id')
  async getOne(@Param('id') id: number): Promise<Smartphone> {
    return this.smartService.findOne(id);
  }
  @Post()
  async create(@Body() Body: Smartphone): Promise<Smartphone> {
    return this.smartService.create(Body);
  }
  @Put(':id')
  async update(
    @Param('id') id: number,
    @Body() Body: Smartphone,
  ): Promise<Smartphone> {
    return this.smartService.update(id, Body);
  }
  @Delete(':id')
  async delete(@Param('id') id: number) {
    return this.smartService.delete(id);
  }
}
