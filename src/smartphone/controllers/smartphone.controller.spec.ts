import { Test, TestingModule } from '@nestjs/testing';
import { SmartphoneController } from './smartphone.controller';

describe('SmartphoneController', () => {
  let controller: SmartphoneController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SmartphoneController],
    }).compile();

    controller = module.get<SmartphoneController>(SmartphoneController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
