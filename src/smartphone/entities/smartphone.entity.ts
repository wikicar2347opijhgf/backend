import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
} from 'typeorm';
@Entity()
export class Smartphone {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  NombreSmart: string;

  @Column()
  Modelo: string;

  @Column('decimal')
  PrecioRef: number;

  @Column('decimal')
  PrecioV: number;

  @Column()
  AnMod: number;

  @CreateDateColumn()
  AudiData: Date;
}
