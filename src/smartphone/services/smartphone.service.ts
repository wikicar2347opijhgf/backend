import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Smartphone } from '../entities/smartphone.entity';

@Injectable()
export class SmartphoneService {
  constructor(
    @InjectRepository(Smartphone) private spRepo: Repository<Smartphone>,
  ) {}
  async findAll(): Promise<Smartphone[]> {
    return this.spRepo.find();
  }
  async findOne(id: number): Promise<Smartphone> {
    return this.spRepo.findOneBy({ id });
  }
  create(body: Smartphone) {
    const newSmart = this.spRepo.create({
      NombreSmart: body.NombreSmart,
      Modelo: body.Modelo,
      PrecioRef: body.PrecioRef,
      PrecioV: body.PrecioV,
      AnMod: body.AnMod,
      AudiData: new Date(),
    });
    return this.spRepo.save(newSmart);
  }
  async update(id: number, body: Smartphone) {
    const smart = await this.spRepo.findOneBy({ id });
    this.spRepo.merge(smart, body);
    return this.spRepo.save(smart);
  }
  async delete(id: number) {
    await this.spRepo.delete({ id });
    return true;
  }
}
