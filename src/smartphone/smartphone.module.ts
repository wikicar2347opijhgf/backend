import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SmartphoneService } from './services/smartphone.service';
import { SmartphoneController } from './controllers/smartphone.controller';
import { Smartphone } from './entities/smartphone.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Smartphone])],
  providers: [SmartphoneService],
  controllers: [SmartphoneController],
})
export class SmartphoneModule {}
